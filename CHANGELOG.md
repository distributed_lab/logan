# Changelog
## Unreleased
### Added
- FromNew function to ease the live of that who use `errors.From(errors.New(...), logan.F{...})` frequently

## 3.8.0
### Changed
- Default formatter now uses time.RFC3339Nano instead of time.RFC3339 