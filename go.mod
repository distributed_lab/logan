module gitlab.com/distributed_lab/logan/v3

go 1.19

require (
	github.com/getsentry/sentry-go v0.20.0
	github.com/go-errors/errors v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.9.0
)

require (
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
)
